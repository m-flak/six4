%ifndef KERNEL32_INC
%define KERNEL32_INC

extern CreateFileW
CreateFile equ CreateFileW
extern GetModuleFileNameW
GetModuleFileName equ GetModuleFileNameW
extern GetModuleHandleW
GetModuleHandle equ GetModuleHandleW
extern GetProcessHeap
extern GetCommandLineW
GetCommandLine equ GetCommandLineW
extern GetTempPathW
GetTempPath equ GetTempPathW
extern GetTempFileNameW
GetTempFileName equ GetTempFileNameW
extern HeapAlloc
extern HeapCreate
extern HeapFree
extern SetHandleInformation
extern VirtualProtect
extern WriteFile

%endif
