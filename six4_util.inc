%ifndef SIX4_UTIL_INC
%define SIX4_UTIL_INC

; RCX = if not -1, will hold heap handle
;		otherwise, it will use processheap
; RDX = if not -1, will hold heap memory pointer
;		otherwise, it store it at [R9] or fail
; R8D = size of heap memory. optional
; R9  = where to store heap pointer if rdx is -1
extern _GetMeHeap
GetMeHeap equ _GetMeHeap

%endif
