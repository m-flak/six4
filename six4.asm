BITS 64

%include "kernel32.inc"

%include "structs.inc"
%include "six4_util.inc"


SECTION .data align=8
prefix_str: db 'S',00h,'1',00h,'X',00h,00h,00h,00h
some_effs:  dd 0xFFFFFFFF

strlen_ws2_loc equ $-$$
strlen_ws2: dw 0x01BC, 0x000C	; string table id, length + null shud b even

strlen_list: dq strlen_ws2_loc

SECTION _bss BSS align=8
temporaryhandle: resq 1
copybufferpointer: resq 1
la_elfa_nuevo:	 resd 1
exefilename:	resb 528 ; max_path in uni
tempdirect:		resb 528 ; ^
tempfile:		resb 528

numbsections:		resq 1
sectiontable:		resq 1


SECTION .text

GLOBAL entry_point
PROC_FRAME entry_point
	db	0x48
	push rbp
	[pushreg rbp]
	mov rbp, rsp
	[setframe rbp, 0]
	sub rsp, 40h ; 64 BYTES
	[allocstack 40h]
[endprolog]
	xor rcx, rcx
	push rcx
	call GetModuleHandle
	mov qword [rbp-8], rax
	pop rcx
	call GetCommandLine
	mov qword [rbp-16], rax
	push 0
	pop r9			; nShowCmd
	mov rdx, r9		; hPrevInstance
	mov r8, qword [rbp-16] ; lpCmdLine
	mov rcx, qword [rbp-8] ; hinstance
	xor rsi, rsi
	xor r15, r15
	call WinMain
	
	xor rax, rax
	
	add rsp, 40h
	mov rsp, rbp
	pop rbp
	retn
ENDPROC_FRAME
	
Global WinMain:function
PROC_FRAME WinMain
	db	0x48
	push rbp
	[pushreg rbp]
	mov r15b, 80h
	sub rsp, r15 ; 128 BYTES
	[allocstack 80h]	
	mov rbp, rsp
	[setframe rbp, 0]
	mov [rbp], r15
	[savereg r15, 0]
	mov [rbp+8], rcx	;hinstance
	[savereg rcx, 8]
	mov [rbp+16], rdx	;hPrevInstance
	[savereg rdx, 16]
	mov [rbp+24], r8	;lpCmdLine
	[savereg r8, 24]
	mov [rbp+32], r9	;ncmdshow
	[savereg r9, 32]
	lea rdi, [rbp+40]	; rdi     - exe full path
	[savereg rsi, 40] ; rdi+8   -  buffer size of 528
	[savereg rsi, 48] ; rdi+16 - temporary directory
	[savereg rsi, 56] ; +24		- temporary file name
	[savereg rsi, 64] ; +32		- size of this exe
	mov r15w, word 64
[endprolog]
	sub rsp, 40h					; SPILL SPACE + extra stack space for stuff
	
	sub [rdi-40], r15
	mov  bx, word 72				; 8 pushes & offset +8
	mov rcx, [rsp+rbx] ; hmodule
	lea rdx, [rel exefilename]
	mov [rdi], rdx
	mov r8d, dword 528
	mov [rdi+8], r8
	call GetModuleFileName
	xor rcx, rcx
	mov rcx, [rdi+8]
	lea rdx, [rel tempdirect]
	mov [rdi+16], rdx
	call GetTempPath
	xor eax, dword 0
	jnz wm_allgood
	nop						;TODO: Handle error
wm_allgood:
	mov rcx, [rdi+16]
	lea rdx, [rel prefix_str]
	xor r8, r8
	lea r9, [rel tempfile]
	mov [rdi+24], r9
	call GetTempFileName
	
	; Get the size of this executable
	; ;
	; get e_lfanew of this exe
	xor rax, rax
	mov rbx, rax
	mov rsi, [rbp+8]
	push rsi
	push rax
	lea rsi, [rsi+03Ch]	; the e_lfanew offset
	mov r14, [rsi]		; the high dword of r14 will be garbage
	mov eax, dword [rel some_effs]	; so AND the high dword away
	and r14, rax			; ;
	pop rax
	mov eax, r14d
	mov dword [rel la_elfa_nuevo], eax
	add eax, dword 4	; skip the NT HEADER
	pop rsi
	lea rsi, [rsi+rax]
	; rsi is now at the FILE HEADER
	; get # sections & get total size of all except last one
	mov bx, [rsi + img_file_hdr.numberofsections]
	; Store # sections 4 later
	mov [rel numbsections], rbx
	mov [rsp+40], rbx
	dec bx
	imul ebx, img_sect_hdr_size
	; Now go to very last section entry
	xor rax, rax
	xor r11, r11
	push rax
	add eax, img_file_hdr_size	; skip the file header
	add eax, img_opt_hdr_size	; now skip optional
	lea eax, [eax+8*16]			; and data directories
	; Store start of section table for later
	lea r11, [rel sectiontable]
	mov [r11], rsi
	add [r11], rax
	lea r11, [rsi+rax]
	mov [rsp+32], r11
	; continue moving
	add eax, ebx
	lea rsi, [rsi+rax]
	; rsi now at last section table header
	mov ebx, [rsi + img_sect_hdr.sizeofrawdata]
	; get raw address & raw size of last section
	mov eax, [rsi + img_sect_hdr.ptrtorawdata]
	add eax, ebx		; ; ORIGINAL FILE SIZE OBTAINED
	mov [rdi+32], rax ; ; ; STORE EXE SIZE
	
	;; !!!! ADD RSP, 20H LATER AS WE STORE RBX & R11 !!!!
	add rsp, 20h
	
	; create the temp file to dump a copy in
	mov rbx, [rsp]
	mov bl, byte 02h
	inc r10					; at this point in execution, r10 is still clean
	shl r10, 7				; make r10 equal 80h
	lea r10, [r10+rbx*1]	; r10 now 82h
	lea rbx, [rbx+r10-4]	; rbx now 80h
	mov rcx, [rdi+24]		; temp name
	mov rdx, [rsp]
	mov edx, dword 0x001f01ff	; all access
	mov r8,  [rsp]
	mov r8d, dword 0x00000003	; share read & write
	pop r9						; security descriptor
	sub rsp, 20h				; 
	mov [rsp+48], r9			; hTemplate
	mov [rsp+40], r10			; Normal | Hidden
	xor r10, rbx
	mov [rsp+32], r10			; CREATE_ALWAYS
	call CreateFile
	add rsp, 20h
	;; Make Handle inheritable
	mov [rel temporaryhandle], rax
	mov r9, [rbp+16]		;zero out r9
	mov r8, r9				;
	mov rdx, r8				;
	inc r8d						; dwFlags: 1
	inc edx						; HANDLE_FLAG_INHERIT
	mov rcx, rax				; hObject
	call SetHandleInformation
	
	; Allocate Memory to dump current image to
	lea rbx, [rel copybufferpointer]
	xor rcx, rcx
	mov rdx, rcx
	not rdx
	mov r8, [rdi+32]
	mov r9, rbx
	call GetMeHeap
	
	; Enable self-modification of headers
	; Get SizeOfHeaders and enable write
	; access on memory containing PE header
	sub rsp, 10h
	mov rcx, [rbp+8]
	lea rdx, [rsp+8]
	mov r8, img_file_hdr_size
	mov rbx, img_opt_hdr.sizeofheaders
	lea r8, [r8+rbx+1*4]
	xor r9, r9
	call load_store_inheader
	mov rax, [rsp+8]
	mov rcx, [rbp+8]
	xor rdx, rdx
	xor r8, r8
	mov edx, eax
	mov r8d, dword 0x40
	lea r9, [rsp+8]
	call VirtualProtect
	add rsp, 10h
	
	; Change a unused field in the header
	; will be restored after copy
	;  below will get offset from BA+e_lfanew
	xor rax, rax
	add rax, [rbp+8]
	or eax, [rel la_elfa_nuevo]
	mov rbx, [rbp+8]
	or bl, byte 24h		; e_oemid in ze dosh header
	not rbx
	not rax
	sbb rbx, rax
	mov r8d, ebx
	; begin call
	sub rsp, 10h				;SPILL SPACE
	mov [rsp], word 0FFh
	mov rcx, [rbp+8]
	lea rdx, [rsp+8]
	movzx rax, word [rsp]			; set the field to this. program will read this field & others
	mov [rdx], rax					;	to determine status
	not r8						; tell func to sub offsets
	xor r9, r9
	mov r9w, word 1				; STORE
	call load_store_inheader
	add rsp, 10h
	; Copy the image to a buffer
	; The buffer will then be written via WriteFile
	mov rcx, [rel copybufferpointer]
	mov rdx, [rdi+32]
	mov r8, [rbp+8]
	call copy_image_to_memry
	; Write this buffer to tha disk
	;
	sub rsp, 10h
	xor rbx, rbx
	mov rcx, [rel temporaryhandle]		; hfile
	mov rdx, [rel copybufferpointer]	; lpbuffer
	mov r8, [rdi+32]					; bytes to write
	lea r9, [rsp+40]					; bytes written
	mov [r9], rbx
	mov [rsp+32], rbx					; lpoverlapped
	call WriteFile
	add rsp, 10h
	
	add rsp, 20h				; free 32 byte initial spill space
	mov r15, [rbp]
	lea rsp, [rbp+40h]
	add rsp, r15
	pop rbp
	ret
ENDPROC_FRAME

; LEAF FUNCTION TO STORE/RETRIEVE FROM LOADED PE HEADER
; rcx - base address
; rdx - pointer to data
; r8	- offset. NEGATIVE VAL for minus offset
; r9w	- zero=load, one=store
Global load_store_inheader
load_store_inheader:
	; get a zero'd out value
	; w/o overwriting anything
	push rcx
	xor [rsp], rcx
	mov [rsp], rdx
	xor rdx, rdx
	mov edx, [rel la_elfa_nuevo]
	add rdx, rcx
	; we are at e_lfanew
	; now check wether to add or sub
	push r8
	btr r8, 03Fh
	jnc ..@lsi0		; R8 IS NOT A NEGATIVE #
	pop r8
	not r8			; make r8 positive
	sub rdx, r8		; e_lfanew - offset
	pop rcx			; get data ptr back
	push rsi		; ;
	mov r8, rdi		; save dese
	jmp ..@lsi1
..@lsi0:
	pop r8
	add rdx, r8		; e_lfanew + offset
	pop rcx
	push rsi		; save volatile regs
	mov r8, rdi		; ;
..@lsi1:
	test r9w, word 1
	jnz ..@lsi2		; r9w is 1 so STORE [rdx] at calc'd address
	mov rsi, rdx	; no jump means store value at address ...
	mov rdi, rcx	;	; ... into [rdx]
	movsq
	jmp ..@lsi3
..@lsi2:
	mov rsi, rcx
	mov rdi, rdx
	movsq
..@lsi3:
	pop rsi
	mov rdi, r8
	ret

; LEAF FUNCTION TO COPY EXE IMAGE FROM PROCESS MEMORY TO A BUFFER
; rcx - pointer to buffer
; rdx - size to copy
; r8  - image base
; RETURN VALUE: rax = operandsize:sizetocopy
Global _copy_image_to_memry
copy_image_to_memry equ _copy_image_to_memry
_copy_image_to_memry:
	push rsi
	push rdi
	push rdx
	xor [rsp], rdx
	mov r9, rcx
	mov r10, [rsp]
	mov r11, [rsp]
	mov [rsp], r8
	mov r8, rdx
	mov r10d, dword 8
..@citm1:
	xor rdx, rdx
	mov eax, r8d	; size to copy
	mov cl, r11b
	shr r10d, cl
	div r10d
	inc r11d
	cmp edx, 0
	ja ..@citm1
	mov rcx, rax
	pop rsi
	mov rdi, r9
	mov edx, r10d
	test edx, 8
	jz ..@citm2
	rep movsq
	push 8
	jmp ..@citm5
..@citm2:
	test edx, 4
	jz ..@citm3
	rep movsd
	push 4
	jmp ..@citm5
..@citm3:
	test edx, 2
	jz ..@citm4
	rep movsw
	push 2
	jmp ..@citm5
..@citm4:
	rep movsb
	push 1
..@citm5:
	pop rax
	push r8
	mov r8d, dword [rsp]
	shl rax, 20h
	adc rax, r8
	pop r8
	pop rdi
	pop rsi
	ret

	
	
	
	