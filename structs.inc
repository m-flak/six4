%ifndef STRUCTS_INC
%define STRUCTS_INC
	
	struc	img_file_hdr
		.machine:				resw 1
		.numberofsections:		resw 1
		.timedatestamp:			resd 1
		.pointertosymboltable: 	resd 1
		.numberofsymbols:		resd 1
		.sizeofoptionalheader:	resw 1
		.characteristics:		resw 1
	endstruc
	
	struc	img_opt_hdr
		.magic:					resw 1
		.majorlinkver:			resb 1
		.minorlinkver:			resb 1
		.sizeofcode:			resd 1
		.sizeofinitdata:		resd 1
		.sizeofuninitdata:		resd 1
		.addressofentry:		resd 1
		.baseofcode:			resd 1
		.imagebase:				resq 1
		.sectionalignment:		resd 1
		.filealignment:			resd 1
		.majorosver:			resw 1
		.minorosver:			resw 1
		.majorimgver:			resw 1
		.minorimgver:			resw 1
		.majorsubsysver:		resw 1
		.minorsubsysver:		resw 1
		.win32versionval:		resd 1
		.sizeofimage:			resd 1
		.sizeofheaders:			resd 1
		.checksum:				resd 1
		.subsystem:				resw 1
		.dllcharacteristics:	resw 1
		.sizeofstackreserve:	resq 1
		.sizeofstackcommit:		resq 1
		.sizeofheapreserve:		resq 1
		.sizeofheapcommit:		resq 1
		.loaderflags:			resd 1
		.numberofrvaandsizes:	resd 1
	endstruc
	
	struc	img_sect_hdr
		.name:				resb 8
		.misc:				resd 1
		.virtualaddr:		resd 1
		.sizeofrawdata:		resd 1
		.ptrtorawdata:		resd 1
		.ptrtorelocs:		resd 1
		.ptrtolinenums:		resd 1
		.numberofrelocs:	resw 1
		.numberoflinenums:	resw 1
		.characteristics:	resd 1
	endstruc
	
	struc	s4_my_sections
		.prev_st:			resq 1
		.next_st:			resq 1
		.name:				resb 8
		.is_bss:			resd 1
		.rawsize:			resd 1
		.actualaddress:		resq 1
	endstruc
	
	
%endif
