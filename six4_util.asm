BITS 64

%include "kernel32.inc"
%include "structs.inc"

SECTION .text

; RCX = if not -1, will hold heap handle
;		otherwise, it will use processheap
; RDX = if not -1, will hold heap memory pointer
;		otherwise, it store it at [R9] or fail
; R8D = size of heap memory. optional
; R9  = where to store heap pointer if rdx is -1
Global _GetMeHeap:function
PROC_FRAME _GetMeHeap
	db 0x48
	mov [rsp+8], rbx
	[savereg rbx, 8]
	mov [rsp+16], rbp
	[savereg rbp, 16]
	mov [rsp+24], rsi
	[savereg rsi, 24]
	push rdi
	[pushreg rdi]
	push r15
	[pushreg r15]
	push r14
	[pushreg r14]
	sub rsp, 50h
	[allocstack 50h]
[endprolog]
	xor rbx, rbx
	xor rsi, rsi
	xor rdi, rdi
	jmp gmh_checkparams.one

..@p1null:
	mov bl, byte 01h
	jmp gmh_checkparams.two
..@p2null:
	mov bh, byte 01h
	jmp gmh_cpdone

gmh_checkparams:
	.one:
		mov rbp, rcx
		btr rbp, 03Fh
		jc ..@p1null
	.two:
		xor rbp, rbp
		mov rbp, rdx
		btr rbp, 03Fh
		jc ..@p2null
gmh_cpdone:
	xor rbp, rbp
	mov [rsp+24], r9
	mov [rsp+32], rbx
	lea rsi, [rsp+40]
	lea rdi, [rsp+48]
	test bl, bl
	jz gmh_create
	mov rbp, r8
	call GetProcessHeap
	mov [rsi], rax
	mov r15, rcx
	mov r14, rdx
	jmp gmh_alloc
gmh_create:
	mov r15, rcx
	mov r14, rdx
	xor rdx, rdx
	mov rbp, r8
	mov r8, rdx			;dwMaxSize
	mov rcx, qword 4	; HEAP exceptions
	mov edx, ebp		;dwSize
	add ebp, 16		;xtra space
	call HeapCreate
	sub ebp, 16
	mov [rsi], rax

gmh_alloc:
	mov rcx, [rsi]
	mov rdx, qword 0Ch
	mov r8d, ebp
	call HeapAlloc
	mov [rdi], rax
	
	mov rbx, [rsp+32]
	mov r9, [rsp+24]
	test bl, bl
	jnz ..@nxt
	mov r15, [rsi]
	..@nxt:
		test bh, bh
		jnz gmh_preret
		mov r14, [rdi]
		jmp gmh_ret
	.r9out:
		mov rbp, [rdi]
		lock xadd [r9], rbp
	jmp gmh_ret
gmh_preret:
	cmp r9, 0
	jne gmh_alloc.r9out
gmh_ret:
	mov rcx, r15
	mov rdx, r14
	xor rax, rax
	pop r14
	pop r15
	mov rbx, [rsp+60h]
	mov rbp, [rsp+68h]
	mov rsi, [rsp+70h]
	add rsp, 50h
	pop rdi
	ret
ENDPROC_FRAME

; RETARDEDLY COMPLEX WAY TO COPY VIRTUAL SECTIONS AS THEY ARE SPEC'D IN FILE
; ; YET STILL SMALLER THAN ANY C CODE HA AHA HA AHA HAHA
; rcx - existing heap to copy the buffer used by this call to
; rdx - image base
; r8  - number of sections
; r9  - start of section table
GLOBAL _ProperlyCopySects:function
PROC_FRAME _ProperlyCopySects
	db 0x48
	push rbp
	[pushreg rbp]
	push rdi
	[pushreg rdi]
	push rsi
	[pushreg rsi]
	sub rsp, 60h ; 96 bytez -[spillx32]-[framex64]-[initialpushesx32]+
	[allocstack 60h]
	lea rbp, [rsp+20h] ; after spill
	[setframe rbp, 20h]
	mov [rbp], rbx
	[savereg rbx, 20h]
	mov [rbp+8], r12
	[savereg r12, 28h]
	;rbp+24 IS A BUFFER FOR STRUCTS
	;rbp+40 IS SIZE OF TOTAL MEMORY
	;rbp+48 IS BSS SIZE
	;rbp+56 IS HEADER SIZE
	
[endprolog]
	mov rbx, s4_my_sections_size
	push rcx
	mov rcx, r8
	shr rcx, 1
	shl rbx, cl
	pop rcx
	; save our args
	mov [rbp+56], rcx
	mov [rbp+48], rdx
	mov [rbp+40], r8
	mov [rbp+32], r9
	; allocate some heap for structs
	xor rcx, rcx
	inc rcx
	not rcx
	mov rdx, rcx
	mov r8, rbx
	lea r9, [rbp+24]
	call _GetMeHeap
	; restore args
	mov  rcx, [rbp+56]
	mov  rdx, [rbp+48]
	mov  r8, [rbp+40]
	mov  r9, [rbp+32]
	; Populate the next fields of the struct array
	mov rdi, [rbp+24]	
	xor rbx, rbx
	mov r12, r8
	mov rax, s4_my_sections_size
	xor r11, r11
	; The actual loop where next fields will be populated
	pcs_pop_loop1:
		cmp r12d, dword 0
		jna ..@pcs01
		lea rdi, [rdi+rbx*8]
		lea rsi, [rdi + s4_my_sections.next_st]
		add rbx, 5
		mov r10, 5
		imul r10, r8
		lea r10, [r11+r10*8]
		cmp rax, r10	;	;	;	; DO NOT SET NEXT FIELD IF THERE IS NO NEXT
		jnz pcs_pop_loop1.b
		lea r11, [rdi+rbx*8]
	.b:
		lea rdi, [rdi+rbx*8]
		xor rdi, r11			;	; Either rdi == rdi OR rdi == 0
		mov [rsi], rdi
		dec r12
		jmp pcs_pop_loop1

..@pcs01:


	;; GetMeHeap was told to use process heap
	; free that memory here
	call GetProcessHeap
	mov rcx, rax
	mov rdx, 0
	mov r8, [rbp+24]
	call HeapFree

	mov rbx, [rbp]
	mov r12, [rbp+8]
	add rsp, 60h
	pop rsi
	pop rdi
	pop rbp
	ret
ENDPROC_FRAME
	